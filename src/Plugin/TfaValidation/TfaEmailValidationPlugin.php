<?php

namespace Drupal\tfa_email\Plugin\TfaValidation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tfa\Plugin\TfaValidationInterface;
use Drupal\tfa\Plugin\TfaBasePlugin;


/**
 * Class TfaEmailValidationPlugin
 *
 * @package Drupal\tfa_email
 *
 * @TfaValidation(
 *   id = "tfa_email_validation",
 *   label = @Translation("TFA Email Validation Plugin"),
 *   description = @Translation("TFA Email Validation Plugin"),
 *   fallbacks = {
 *    "tfa_recovery_code"
 *   },
 *   isFallback = FALSE
 * )
 */
class TfaEmailValidationPlugin extends TfaBasePlugin implements TfaValidationInterface {

  /**
   * Get TFA process form from plugin.
   *
   * @param array $form
   *   The configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form API array.
   */
  public function getForm(array $form, FormStateInterface $form_state) {
    $message = 'Verification code is application generated and @length digits long.';
    if ($this->getUserData('tfa', 'tfa_recovery_code', $this->uid, $this->userData) && $this->getFallbacks()) {
      $message .= '<br/>Can not access your account? Use one of your recovery codes.';
    }
    $form['code'] = [
      '#type' => 'textfield',
      '#title' => t('Application verification code'),
      '#description' => t($message, ['@length' => $this->codeLength]),
      '#required'  => TRUE,
      '#attributes' => ['autocomplete' => 'off'],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['login'] = [
      '#type'  => 'submit',
      '#value' => t('Verify'),
    ];

    return $form;
  }

  /**
   * Validate form.
   *
   * @param array $form
   *   The configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Whether form passes validation or not
   */
  public function validateForm(array $form, FormStateInterface $form_state) {
    return TRUE;
  }

  /**
   * @param $config
   * @param $state
   *
   * @return array
   */
  public function buildConfigurationForm($config, $state) {
    $settings_form['exp_time'] = [
      '#type' => 'number',
      '#title' => t('Code expiration time'),
      '#default_value' => 60,
      '#description' => 'Number of seconds after which received code will expire',
      '#size' => 2,
      '#states' => $state,
      '#required' => TRUE,
    ];

    $settings_form['email_subjet'] = [
      '#type' => 'textfield',
      '#title' => t('Email Subject'),
      '#default_value' => "OTP is - @otp",
      '#description' => 'Email subject of the email which is triggered for TFA',
      '#states' => $state,
      '#required' => TRUE,
    ];

    $settings_form['email_body'] = [
      '#type' => 'textarea',
      '#title' => t('Email body'),
      '#default_value' => "Your OTP is - @otp",
      '#description' => 'Email body of the email which is triggered for TFA',
      '#states' => $state,
      '#required' => TRUE,
    ];

    return $settings_form;
  }

  /**
   * Get validation plugin fallbacks.
   *
   * @return string[]
   *   Returns a list of fallback methods available for the current validation
   */
  public function getFallbacks() {
    return [];
  }

  /**
   * Is the validation plugin a fallback?
   *
   * If the plugin is a fallback we remove it from the validation
   * plugins list and show it only in the fallbacks list.
   *
   * @return bool
   *   TRUE if plugin is a fallback otherwise FALSE
   */
  public function isFallback() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function ready() {
    return TRUE;
  }

}
